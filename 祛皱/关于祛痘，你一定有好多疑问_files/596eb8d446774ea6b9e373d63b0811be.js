//var domain = 'http://localhost:1307';
var domain = 'http://122.114.120.247:12345';

var trackId = '10818';
var copyTime = 600;//不支持复制的浏览器，按下多少毫秒算为准备复制

$(function () {
    var fingerprint = '';
    var userAgent = '';
    var touchStart = 0;

    //是否支持复制事件
    var div = document.createElement('div');
    var supportCopy = 'ontouchstart' in div;

    new Fingerprint2().get(function (result, components) {
        fingerprint = result;
        userAgent = components[0].value;

        var selector = '.weixin';
        if (userAgent.indexOf('Android') >= 0) {
            //userAgent.indexOf('UCBrowser') >= 0) {
            
            if (false) {//supportCopy
                $(selector).bind('copy', track);
            } else {
                $(selector).bind('touchstart', function (event) {
                    touchStart = new Date().getTime();
                    //event.preventDefault()
                });
                $(selector).bind('touchend', function () {
                    var interval = new Date().getTime() - touchStart;
                    checkTrack(interval);
                });
                $(selector).bind('touchcancel', function () {
                    var interval = new Date().getTime() - touchStart;
                    checkTrack(interval);
                });
            }
        }
        else {
            $(selector).bind('copy', track);
        }
    });

    function checkTrack(interval) {
        if (interval > copyTime) {
            //$('#log').append('<br/>cancel:' + interval);
            track();
        }
    }

    function track() {
        console.log(trackId);
        console.log(fingerprint);

        var weixin = $('.weixin').html();
        console.log(weixin);

        $.ajax({
            type: "get",
            async: false,
            url: domain + "/Track/HandleCopy",
            dataType: "jsonp",
            jsonp: "param",
            jsonpCallback: trackId + '__' + fingerprint + '__' + encodeURI(weixin)+ '__' + encodeURI(userAgent) ,
            success: function (json) {
            },
            error: function () {
            }
        });
    }
})